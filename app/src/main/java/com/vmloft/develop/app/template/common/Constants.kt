package com.vmloft.develop.app.template.common

/**
 * Create by lzan13 on 2020-02-18 14:43
 * 常量类
 */
object Constants {

    const val dbName = "wcdb_vmmatch"
    const val dbPass = "wcdb_vmmatch_123"

    // 用户信息改变事件
    const val userInfoEvent = "userInfoEvent"

    // 发布新内容
    const val createPostEvent = "createPostEvent"

}